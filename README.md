WARNING : Sound had been implemented just using Direct Sound, not FMOD or wwise, so the game does not have SOUND SAMPLE SAFEGUARD SYSTEM - meaning if the the game does not fill enough sound samples per frame(i.e due to performace issue),
or if you are using a monitor with high framerate, you will definetely hear cracking sound. I wanted to implement a reliable sound system, but there wansn't enough time to do so.

1. What is this game?
'Laser Is Dangerous' is a fast paced 2D laser-dodging & shooting game where you have to fight with waves of enemies. 
Move against gravity while avoid everything - including the enemies, lasers, mountains, and walls.

The engine itself is greatly inspired by handmade hero, so most of the components except the OpenGL - which was used for displaying the final buffer - are built by myself.

2. Things that I worked on this game
- Software Renderer using SIMD with minimum OpenGL support
- Sparse world system for Optimization
- Basic 2D Physics
- Asset Compressor & Multi-threathed Asset Loader
- Win32 Platform Layer
