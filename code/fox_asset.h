/******************************************************************************
Author: GyuHyeon Lee
Email:  email: weanother@gmail.com

Github : https://LeeGyuHyun@bitbucket.org/LeeGyuHyun/fox.git

(C) Copyright by GyuHyeon, Lee. All Rights Reserved. $
******************************************************************************/
#ifndef FOX_ASSET_H
#define FOX_ASSET_H

#include "fox_asset_file_formats.h"

struct loaded_sound
{
    u32 channelCount;
    u32 sampleCount;
    int16 *samples[2];
};

enum asset_state
{
    Asset_State_NotLoaded,
    Asset_State_Queued,
    Asset_State_Loaded,
};
struct asset_slot // NOTE : Struct that actually holds the asset
{
    asset_state state;

    union
    {
        loaded_bitmap *bitmap;
        loaded_sound *sound;
    };
};
struct asset_match_vector
{
    r32 e[Asset_Tag_Count];
};

struct asset_weight_vector
{
    r32 e[Asset_Tag_Count];
};

struct asset_tag
{
    // This means any character of this asset
    // tallness, shiness... and the value means how much it is 'character'
    u32 ID;
    r32 value;
};

struct asset_type
{
    u32 firstAssetID;
    u32 onePastLastAssetID;
};

struct asset_file
{
    platform_file_handle handle;
    fea_header header;

    u32 tagBase;
    u32 assetBase;
};

struct game_assets
{
    memory_arena arena;

    // TODO : Not thrilled about this back-pointer
    // NOTE : this is needed for the background tasks
    struct transient_state *tranState;

    // We can use this to look up all the assets with same types
    asset_type assetTypes[Asset_Type_Count];

    u32 tagCount;
    fea_tag *tags;
    // Value of the tag should not be bigger than this one
    r32 tagMaxRange[Asset_Tag_Count];

    u32 assetCount;
    asset_slot *assetSlots;
    fea_asset *assets;

    u8* feaContents;
    u32 fileCount;
    asset_file *files;
};

inline loaded_bitmap *
GetLoadedBitmap(game_assets *assets, u32 ID)
{
    loaded_bitmap *result = assets->assetSlots[ID].bitmap;
    return result;
}

inline loaded_sound *
GetLoadedSound(game_assets *assets, u32 ID)
{
    loaded_sound *result = assets->assetSlots[ID].sound;
    return result;
}

inline fea_bitmap_info *
GetBitmapInfo(game_assets *assets, u32 ID)
{
    fea_bitmap_info *result = &assets->assets[ID].bitmapInfo;

    return result;
}

inline fea_sound_info *
GetSoundInfo(game_assets *assets, u32 ID)
{
    fea_sound_info *result = &assets->assets[ID].soundInfo;

    return result;
}

// These functions should be called externally
internal void LoadBitmapAsset(game_assets *assets, u32 ID);
inline void 
PrefetchBitmapAsset(game_assets *assets, u32 ID)
{
    LoadBitmapAsset(assets, ID);
}
internal void LoadSoundAsset(game_assets *assets, u32 ID);
inline void 
PrefetchSoundAsset(game_assets *assets, u32 ID)
{
    LoadSoundAsset(assets, ID);
}

#endif