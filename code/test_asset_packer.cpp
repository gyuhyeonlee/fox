#include <stdio.h>
#include <stdlib.h>
#include "test_asset_packer.h"

#pragma pack(push, 1)
struct bitmap_header
{
    uint16 fileType;
    uint32 fileSize;
    uint16 reserved1;
    uint16 reserved2;
    uint32 bitmapOffset;
    uint32 size;
    int32 width;
    int32 height;
    uint16 planes;
    uint16 bitsPerPixel;
    uint32 compression;
    uint32 sizeOfBitmap;
    int32 horzResolution;
    int32 vertResolution;
    uint32 colorsUsed;
    uint32 colorsImportant;

    uint32 redMask;
    uint32 greenMask;
    uint32 blueMask;
};

#define ProduceWaveID(a, b, c, d) (((uint32)(a) << 0) | ((uint32)(b) << 8) | ((uint32)(c) << 16) | ((uint32)(d) << 24)) 
enum
{
    WAVE_ChunkID_fmt = ProduceWaveID('f', 'm', 't', ' '),
    WAVE_ChunkID_RIFF = ProduceWaveID('R', 'I', 'F', 'F'),
    WAVE_ChunkID_data = ProduceWaveID('d', 'a', 't', 'a'),
    WAVE_ChunkID_WAVE = ProduceWaveID('W', 'A', 'V', 'E'),
};

struct WAVE_header
{
    uint32 riffID;
    uint32 size;
    uint32 waveID;
};

struct WAVE_fmt
{
    uint16 wFormatTag;
    uint16 nChannels;
    uint32 nSamplesPerSec;  
    uint32 nAvgBytesPerSec;
    uint16 nBlockAlign;
    uint16 wBitsPerSample;
    uint16 cbSize;
    uint16 wValidBitsPerSample;
    uint32 dwChannelMask;
    uint8 subFormat[16];
};

struct WAVE_chunk
{
    uint32 ID;
    uint32 size;
};

#pragma pack(pop)
// End the exact fitting and back to the regular packing

struct loaded_bitmap
{
    int32 width;
    int32 height;
    int32 pitch;
    void *memory;

    void *freeThis;
};

struct loaded_sound
{
    u32 channelCount;
    u32 sampleCount;
    int16 *samples[2];

    void *freeThis;
};

struct read_file_result
{
    u32 contentSize;
    void *content;
};

internal read_file_result
ReadEntireFile(char *fileName)
{
    read_file_result result = {};

    FILE *source = fopen(fileName, "rb");

    if(source)
    {
        fseek(source, 0, SEEK_END);
        result.contentSize = ftell(source);
        fseek(source, 0, SEEK_SET);

        result.content = malloc(result.contentSize);
        fread(result.content, result.contentSize, 1, source);
        fclose(source);
    }
    else
    {
        printf("ERROR : Cannot open file %s.\n", fileName);
    }

    return result;
}

// NOTE : Align is based on left bottom corner as Y is up -> which means top-down
internal loaded_bitmap
LoadBMP(char *fileName, v2 alignPercentage = V2(0.5f, 0.5f))
{
    loaded_bitmap result = {};

    read_file_result readResult = ReadEntireFile(fileName);

    if(readResult.contentSize != 0)
    {
        result.freeThis = readResult.content;
        bitmap_header *bitmapHeader = (bitmap_header *)readResult.content;
        uint32 *pixels = (uint32 *)((uint8 *)readResult.content + bitmapHeader->bitmapOffset);

        result.memory = pixels;

        result.width = bitmapHeader->width;
        result.height = bitmapHeader->height;

        // NOTE : This function is only for bottom up bmps for now.
        Assert(result.height >= 0);
        // TODO : Do this for each compression type?
        Assert(bitmapHeader->compression == 3);

        // If you are using this generically,
        // the height will be negative for top-down
        // please remember that BMP files can go in either direction and
        // (there can be compression, etc... don't think this 
        // is complete BMP loading function)

        // NOTE : Byte Order in memory is determined by the header itself,
        // so we have to read out the masks and convert our piexels ourselves.
        uint32 redMask = bitmapHeader->redMask;
        uint32 greenMask = bitmapHeader->greenMask;
        uint32 blueMask = bitmapHeader->blueMask;
        uint32 alphaMask = ~(redMask | greenMask | blueMask);

        // Find out how many bits we must rightshift for each colormask
        bit_scan_result redScan = FindLeastSignificantSetBit(redMask);
        bit_scan_result greenScan = FindLeastSignificantSetBit(greenMask);
        bit_scan_result blueScan = FindLeastSignificantSetBit(blueMask);
        bit_scan_result alphaScan = FindLeastSignificantSetBit(alphaMask);

        Assert(redScan.found && greenScan.found && blueScan.found && alphaScan.found);

        int32 alphaShift = (int32)alphaScan.index;
        int32 redShift = (int32)redScan.index;
        int32 greenShift = (int32)greenScan.index;
        int32 blueShift = (int32)blueScan.index;
        
        uint32 *sourceDest = pixels;

        for(int32 y = 0;
            y < bitmapHeader->height;
            ++y)
        {
            for(int32 x = 0;
                x < bitmapHeader->width;
                ++x)
            {
                uint32 color = *sourceDest;

                v4 texel = {(real32)((color & redMask) >> redShift),
                    (real32)((color & greenMask) >> greenShift),
                    (real32)((color & blueMask) >> blueShift),
                    (real32)((color & alphaMask) >> alphaShift)};

                    texel = SRGB255ToLinear1(texel);

                // NOTE : Premultiplied Alpha!
                    texel.rgb *= texel.a;

                    texel = Linear1ToSRGB255(texel);

                    *sourceDest++ = (((uint32)(texel.a + 0.5f) << 24) |
                     ((uint32)(texel.r + 0.5f) << 16) |
                     ((uint32)(texel.g + 0.5f) << 8) |
                     ((uint32)(texel.b + 0.5f) << 0));
            }
        }        
    }

// NOTE : Because the usual bmp format is already bottom up, don't need a prestep anymore!
    result.pitch = result.width*BITMAP_BYTES_PER_PIXEL;

// There might be top down bitmaps. Use this method for them
#if 0
// Because we want to go to negative, set it to negative value
result.pitch = -result.width*BITMAP_BYTES_PER_PIXEL;

// Readjust the memory to be the start of the last row
// because the bitmap that was read is upside down. 
// Therefore, we need to read backward
result.memory = (uint8 *)result.memory - result.pitch*(result.height - 1);
#endif
    return result;
}

struct riff_iterator
{
    uint8 *at;
    uint8 *end;
};

inline riff_iterator
ParseChunkAt(void *at, void *end)
{
    riff_iterator result = {};

    result.at = (uint8 *)at;
    result.end = (uint8 *)end;

    return result;
}

inline riff_iterator
NextChunk(riff_iterator iter)
{
    WAVE_chunk *currentChunk = (WAVE_chunk *)iter.at;

    // NOTE : Because of the padding inside the wav file
    uint32 size = (currentChunk->size + 1) & ~1;
    iter.at += sizeof(WAVE_chunk) + size;

    return iter;
}

inline uint32
GetChunkID(riff_iterator iter)
{
    WAVE_chunk *chunk = (WAVE_chunk *)iter.at;

    return chunk->ID;
}

inline void *
GetChunkData(riff_iterator iter)
{
    void *result = (WAVE_chunk *)(iter.at + sizeof(WAVE_chunk));

    return result;
}

inline uint32
GetChunkDataSize(riff_iterator iter)
{
    WAVE_chunk *chunk = (WAVE_chunk *)iter.at;

    return chunk->size;
}

inline bool32
IsValid(riff_iterator iter)
{
    bool32 result = (iter.at < iter.end);

    return result;
}

internal loaded_sound
LoadWAV(char *fileName, uint32 sectionFirstSampleIndex, uint32 sectionSampleCount)
{
    loaded_sound result = {};

    read_file_result readResult = ReadEntireFile(fileName);

    // NOTE : Should not be 0.
    Assert(readResult.contentSize != 0);

    if(readResult.contentSize != 0)
    {
        result.freeThis = readResult.content;
        // Master chunk
        WAVE_header *waveHeader = (WAVE_header *)readResult.content;
        Assert(waveHeader->riffID == WAVE_ChunkID_RIFF);
        Assert(waveHeader->waveID == WAVE_ChunkID_WAVE);

        uint32 channelCount = 0;
        int16 *sampleData = 0;
        uint32 sampleDataSize = 0;
        uint32 sampleCount = 0;

        // Loop through sub chunks for the wave file
        for(riff_iterator iter = ParseChunkAt(waveHeader + 1, (uint8 *)(waveHeader + 1) + waveHeader->size - 4);
            IsValid(iter);
            iter = NextChunk(iter))
        {
            switch(GetChunkID(iter))
            {
                case WAVE_ChunkID_fmt:
                {
                    WAVE_fmt *format = (WAVE_fmt *)GetChunkData(iter); 

                    /* NOTE : We will only support file with this format
                    pcm data type
                    stereo channel
                    bits per sample = 16
                    samples per second = 48000
                    */ 
                    Assert(format->wFormatTag == 1);
                    Assert(format->nChannels == 2);
                    Assert(format->wBitsPerSample == 16);
                    Assert(format->nSamplesPerSec == 48000);

                    channelCount = format->nChannels;
                }break;
 
                case WAVE_ChunkID_data:
                {
                    sampleData = (int16 *)GetChunkData(iter);
                    sampleDataSize = GetChunkDataSize(iter);
                }break;
            }
        }

        Assert(channelCount && sampleData);

        result.channelCount = channelCount;
        result.sampleCount = sampleDataSize / (channelCount * sizeof(int16));

        if(result.channelCount == 1)
        {
            result.samples[0] = sampleData;
            result.samples[1] = 0;
        }
        else if(result.channelCount == 2)
        {
            result.samples[0] = sampleData;
            // If the sound was stereo, there will be twice more samples;
            // so we should just add sampleCount.
            result.samples[1] = sampleData + result.sampleCount;

            // TODO : This swapping function makes right channel samples totally busted.
            for(uint32 sampleID = 0;
                sampleID < result.sampleCount;
                ++sampleID)
            {
                int16 source = sampleData[2 * sampleID];
                sampleData[2*sampleID] = sampleData[sampleID];
                sampleData[sampleID] = source;
            }
        }
        else
        {
            InvalidCodePath;
        }

        // TODO : Load right channels!
        result.channelCount = 1;

        // NOTE : If there is a notaion about where to play
        if(sectionSampleCount)
        {
            Assert(sectionFirstSampleIndex + sectionSampleCount <= result.sampleCount);
            result.sampleCount = sectionSampleCount;

            for(uint32 channelIndex = 0;
                channelIndex < result.channelCount;
                ++channelIndex)
            {
                result.samples[channelIndex] += sectionFirstSampleIndex;
            }
        }
    }

    return result;
}


internal void
BeginGettingAssetsBasedOnType(game_assets *assets, asset_type_id assetTypeID)
{
    assets->DEBUGSelectedType = assets->types + assetTypeID;
    fea_type *assetType = assets->DEBUGSelectedType; 

    assetType->typeID = assetTypeID;
    // Initially the each asset type has no asset!
	assetType->firstAssetID = assets->assetCount;
	assetType->onePastLastAssetID = assets->assetCount;
}

internal void
AddBitmapAsset(game_assets *assets, char *fileName, r32 alignPercentage0 = 0.5f, r32 alignPercentage1 = 0.5f)
{
    assets->DEBUGSelectedAsset = assets->assets + assets->assetCount;
	fea_asset *taggedAsset = assets->DEBUGSelectedAsset;

	taggedAsset->firstTagID = assets->tagCount++;
	taggedAsset->onePastLastTagID = assets->tagCount;

    // Assert(assets->DEBUGUsedAssetCount < assets->assetCount); 
    taggedAsset->bitmapInfo.alignPercentage.x = alignPercentage0;
    taggedAsset->bitmapInfo.alignPercentage.y = alignPercentage1;

    asset_load_info *loadInfo = assets->loadInfos + assets->assetCount;
    loadInfo->fileName = fileName;
    loadInfo->type = Asset_Type_Bitmap;

    ++assets->DEBUGSelectedType->onePastLastAssetID;
    ++assets->assetCount;
}

internal u32
AddSoundAsset(game_assets *assets, char *fileName, uint32 sectionFirstSampleIndex = 0, 
                    uint32 sectionSampleCount = 0)
{
    assets->DEBUGSelectedAsset = assets->assets + assets->assetCount;
    fea_asset *taggedAsset = assets->DEBUGSelectedAsset;

    taggedAsset->firstTagID = assets->tagCount++;
    taggedAsset->onePastLastTagID = assets->tagCount;

    // Assert(assets->DEBUGUsedAssetCount < assets->assetCount); 

    taggedAsset->soundInfo.firstSampleIndex = sectionFirstSampleIndex;
    taggedAsset->soundInfo.samplesToPlay = sectionSampleCount; 

    asset_load_info *loadInfo = assets->loadInfos + assets->assetCount;
    loadInfo->fileName = fileName;
    loadInfo->type = Asset_Type_Sound;

    ++assets->DEBUGSelectedType->onePastLastAssetID;
    return assets->assetCount++;
}

internal void
EndGettingAssetsBasedOnType(game_assets *assets)
{
}

internal void
AddTag(game_assets *assets, asset_tag_id tagID, real32 value)
{
    // TODO : How to do this?
    ++assets->DEBUGSelectedAsset->onePastLastTagID;
    fea_tag *tag = assets->tags + assets->tagCount++; 

    tag->ID = tagID;
    tag->value = value;
}

FILE *out = 0;
int 
main(int argc, char **argv)
{
	game_assets gameAssets_ = {};
	game_assets *gameAssets = &gameAssets_;

    gameAssets->tagCount = 0;
    gameAssets->assetCount = 1;

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_Square);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/square.bmp");
    EndGettingAssetsBasedOnType(gameAssets);

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_Number);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/number_0.bmp");
    AddTag(gameAssets, Asset_Tag_Numbering, 0.0f);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/number_1.bmp");
    AddTag(gameAssets, Asset_Tag_Numbering, 1.0f);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/number_2.bmp");
    AddTag(gameAssets, Asset_Tag_Numbering, 2.0f);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/number_3.bmp");
    AddTag(gameAssets, Asset_Tag_Numbering, 3.0f);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/number_4.bmp");
    AddTag(gameAssets, Asset_Tag_Numbering, 4.0f);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/number_5.bmp");
    AddTag(gameAssets, Asset_Tag_Numbering, 5.0f);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/number_6.bmp");
    AddTag(gameAssets, Asset_Tag_Numbering, 6.0f);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/number_7.bmp");
    AddTag(gameAssets, Asset_Tag_Numbering, 7.0f);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/number_8.bmp");
    AddTag(gameAssets, Asset_Tag_Numbering, 8.0f);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/number_9.bmp");
    AddTag(gameAssets, Asset_Tag_Numbering, 9.0f);
    EndGettingAssetsBasedOnType(gameAssets);

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_Tutorial);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/tutorial.bmp");
    EndGettingAssetsBasedOnType(gameAssets);

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_GameLogo);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/game_logo.bmp");
    EndGettingAssetsBasedOnType(gameAssets);

    // BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_DigipenLogo);
    // AddBitmapAsset(gameAssets, "../fox/data/LIDasset/digipen_logo.bmp");
    // EndGettingAssetsBasedOnType(gameAssets);

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_TeamLogo);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/team_logo.bmp");
    EndGettingAssetsBasedOnType(gameAssets);

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_Wave);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/wave.bmp");
    EndGettingAssetsBasedOnType(gameAssets);

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_NextWaveIn);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/next_wave_in.bmp");
    EndGettingAssetsBasedOnType(gameAssets);

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_NextWeapon);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/next_weapon.bmp");
    EndGettingAssetsBasedOnType(gameAssets);

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_ExclamationMark);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/exclamation_mark.bmp");
    EndGettingAssetsBasedOnType(gameAssets);

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_Arrow);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/arrow.bmp");
    EndGettingAssetsBasedOnType(gameAssets);

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_ArrowIndicator);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/arrow_indication.bmp");
    EndGettingAssetsBasedOnType(gameAssets);

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_PressR);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/press_r.bmp");
    EndGettingAssetsBasedOnType(gameAssets);

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_NextWeaponIndicator);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/laser_single.bmp");
    AddTag(gameAssets, Asset_Tag_WeaponType, 0.0f);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/laser_triple.bmp");
    AddTag(gameAssets, Asset_Tag_WeaponType, 1.0f);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/laser_backward.bmp");
    AddTag(gameAssets, Asset_Tag_WeaponType, 2.0f);
    EndGettingAssetsBasedOnType(gameAssets);

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_Credit);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/credit_1.bmp");
    AddTag(gameAssets, Asset_Tag_Sequence, 0.0f);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/credit_2.bmp");
    AddTag(gameAssets, Asset_Tag_Sequence, 1.0f);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/credit_3.bmp");
    AddTag(gameAssets, Asset_Tag_Sequence, 2.0f);
    EndGettingAssetsBasedOnType(gameAssets);

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_Thrust);
    AddBitmapAsset(gameAssets, "../fox/data/test2/thrust.bmp");
    EndGettingAssetsBasedOnType(gameAssets);

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_Aim);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/aim.bmp");
    AddTag(gameAssets, Asset_Tag_WeaponType, 0.0f);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/aim_shotgun.bmp");
    AddTag(gameAssets, Asset_Tag_WeaponType, 1.0f);
    EndGettingAssetsBasedOnType(gameAssets);

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_Shield);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/shield.bmp");
    EndGettingAssetsBasedOnType(gameAssets);

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_Tree);
    AddBitmapAsset(gameAssets, "../fox/data/test2/tree00.bmp", 0.493827164f, 0.295652181f);
    EndGettingAssetsBasedOnType(gameAssets);

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_Shadow);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/thrust.bmp");
    EndGettingAssetsBasedOnType(gameAssets);

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_Sword);
    AddBitmapAsset(gameAssets, "../fox/data/test2/attack.bmp", 0.5f, 0.65625f);
    EndGettingAssetsBasedOnType(gameAssets);

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_RaycastAttack);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/raycast_attack.bmp");
    EndGettingAssetsBasedOnType(gameAssets);

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_LaserSurrounding);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/laser_particle.bmp");
    EndGettingAssetsBasedOnType(gameAssets);

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_Cloud);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/cloud_2.bmp");
    EndGettingAssetsBasedOnType(gameAssets);

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_Sun);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/sun_3.bmp");
    EndGettingAssetsBasedOnType(gameAssets);

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_Mountain);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/mountain.bmp");
    EndGettingAssetsBasedOnType(gameAssets);

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_Ship);
    AddBitmapAsset(gameAssets, "../fox/data/test2/ship.bmp", 0.493827164f, 0.8f);
    EndGettingAssetsBasedOnType(gameAssets);   

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_Ship);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/ship_side.bmp");
    AddTag(gameAssets, Asset_Tag_FacingDirection, 0.0f);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/ship_diagonal.bmp");
    AddTag(gameAssets, Asset_Tag_FacingDirection, 1.0f);
    AddBitmapAsset(gameAssets, "../fox/data/LIDasset/ship_normal.bmp");
    AddTag(gameAssets, Asset_Tag_FacingDirection, 2.0f);
    EndGettingAssetsBasedOnType(gameAssets);

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_LaserSound);
    AddSoundAsset(gameAssets, "../fox/data/LIDasset/sound/laser_attack_sound2.wav");
    EndGettingAssetsBasedOnType(gameAssets);

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_BoostSound);
    AddSoundAsset(gameAssets, "../fox/data/LIDasset/sound/boost_sound_2.wav");
    EndGettingAssetsBasedOnType(gameAssets);

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_EnemyLaserSound);
    AddSoundAsset(gameAssets, "../fox/data/LIDasset/sound/enemy_laser_1.wav");
    EndGettingAssetsBasedOnType(gameAssets);

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_EnemyAwareSound);
    AddSoundAsset(gameAssets, "../fox/data/LIDasset/sound/enemy_aware_sound.wav");
    EndGettingAssetsBasedOnType(gameAssets);

    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_EnemyDeathSound);
    AddSoundAsset(gameAssets, "../fox/data/LIDasset/sound/enemy_death_3.wav");
    EndGettingAssetsBasedOnType(gameAssets);

#if 1
    BeginGettingAssetsBasedOnType(gameAssets, Asset_Type_Music);
    uint32 oneSoundChunk = 5*48000;
    u32 lastLoadedSoundID = 0;

    uint32 totalSoundSamples = 10842131;
    for(uint32 firstSampleIndex = 0;
        firstSampleIndex < totalSoundSamples;
        firstSampleIndex += oneSoundChunk)
    {
        uint32 samplesToLoad = totalSoundSamples - firstSampleIndex;
        if(samplesToLoad > oneSoundChunk)
        {
            samplesToLoad = oneSoundChunk;
        }

        u32 thisLoadedSoundID = AddSoundAsset(gameAssets, "../fox/data/LIDasset/sound/main.wav", firstSampleIndex, samplesToLoad);

        if(lastLoadedSoundID)
        {
            gameAssets->assets[lastLoadedSoundID].soundInfo.nextIDToPlay = thisLoadedSoundID;
        }

        lastLoadedSoundID = thisLoadedSoundID;
    }
    EndGettingAssetsBasedOnType(gameAssets);
#endif

    out = fopen("test.fea", "wb");
    if(out)
    {
        fea_header header = {};
        header.magicValue = FEA_MAGIC_VALUE;
        header.version = FEA_ASSET_VERSION;
        header.tagCount = gameAssets->tagCount;
        header.assetCount = gameAssets->assetCount;

        u32 tagArraySize = header.tagCount*sizeof(fea_tag);
        u32 assetTypeArraySize = Asset_Type_Count*sizeof(fea_type);
        u32 assetArraySize = header.assetCount*sizeof(fea_asset);

        // NOTE : In memory, tag array will be located right after the header
        header.tagOffset = sizeof(header);
        header.assetTypeOffset = header.tagOffset + tagArraySize;
        header.assetInfoOffset = header.assetTypeOffset + assetTypeArraySize;

        fwrite(&header, sizeof(header), 1, out);
        fwrite(gameAssets->tags, tagArraySize, 1, out);
        fwrite(gameAssets->types, assetTypeArraySize, 1, out);

        // NOTE : Move the file pointer += assetArraySize
        fseek(out, assetArraySize, SEEK_CUR);

        for(u32 assetIndex = 1; // Starting from 1 because 0 is the null asset
            assetIndex < header.assetCount;
            ++assetIndex)
        {
            asset_load_info *loadInfo = gameAssets->loadInfos + assetIndex;
            fea_asset *asset = gameAssets->assets + assetIndex;

            asset->dataOffset = ftell(out);
            if(loadInfo->type == Asset_Type_Bitmap)
            {
                // Load BMP
                loaded_bitmap loadedBitmap = LoadBMP(loadInfo->fileName);

                asset->bitmapInfo.dim[0] = loadedBitmap.width;
                asset->bitmapInfo.dim[1] = loadedBitmap.height;

                Assert((loadedBitmap.width*4) == loadedBitmap.pitch);
                fwrite(loadedBitmap.memory, loadedBitmap.width*loadedBitmap.height*BITMAP_BYTES_PER_PIXEL, 1, out);

                free(loadedBitmap.freeThis);
            }
            else
            {
                Assert(loadInfo->type == Asset_Type_Sound);

                // Load WAV
                loaded_sound loadedSound = LoadWAV(loadInfo->fileName, 
                                                asset->soundInfo.firstSampleIndex, 
                                                asset->soundInfo.samplesToPlay);
                asset->soundInfo.sampleCount = loadedSound.sampleCount;
                asset->soundInfo.channelCount = loadedSound.channelCount;

                for(u32 channelIndex = 0;
                    channelIndex < asset->soundInfo.channelCount;
                    ++channelIndex)
                {
                    fwrite(loadedSound.samples[channelIndex], asset->soundInfo.sampleCount*sizeof(i16), 1, out);
                }

                free(loadedSound.freeThis);
            }
        }

        fseek(out, (u32)header.assetInfoOffset, SEEK_SET);
        fwrite(gameAssets->assets, assetArraySize, 1, out);

        fclose(out);
    }

	return 0;
}
