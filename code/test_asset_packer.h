#ifndef TEST_ASSET_PACKER_H
#define TEST_ASSET_PACKER_H

#include "fox_platform.h"
#include "fox_asset_id.h"
#include "fox_asset_file_formats.h"
#include "fox_intrinsics.h"
#include "fox_math.h"

enum asset_type
{
    Asset_Type_Bitmap,
    Asset_Type_Sound,
};

struct asset_load_info
{
    asset_type type;
    char *fileName;
};

#define VERY_LARGE_NUMBER 4096
struct game_assets
{
    u32 tagCount;
    u32 assetTypeCount;
    u32 assetCount;

    fea_tag tags[VERY_LARGE_NUMBER];
    fea_type types[Asset_Type_Count];

    asset_load_info loadInfos[VERY_LARGE_NUMBER];
    fea_asset assets[VERY_LARGE_NUMBER];

    fea_type *DEBUGSelectedType;
    fea_asset *DEBUGSelectedAsset;
};


#endif