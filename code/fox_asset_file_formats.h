#ifndef FOX_ASSET_FILE_FORMATS_H
#define FOX_ASSET_FILE_FORMATS_H

#pragma pack(push, 1)

#define ProduceMagicValue(a, b, c, d) (((uint32)(a) << 0) | ((uint32)(b) << 8) | ((uint32)(c) << 16) | ((uint32)(d) << 24)) 

#define FEA_MAGIC_VALUE ProduceMagicValue('f', 'e', 'a', 'f')
#define FEA_ASSET_VERSION 0

struct fea_bitmap_info
{
	u32 dim[2];
    v2 alignPercentage;
};

struct fea_sound_info
{
    u32 firstSampleIndex;
    u32 samplesToPlay; 
    u32 nextIDToPlay;

    u32 sampleCount;
    u32 channelCount;
};

struct fea_header
{
	u32 magicValue;
	u32 version;

	u32 tagCount;
	u32 assetCount;

	// NOTE : These are the offsets(inside the memory) 
	// of the arrays
	u64 tagOffset;
	u64 assetTypeOffset;
	u64 assetInfoOffset;
};

struct fea_tag
{
	u32 ID;
	r32 value;
};

struct fea_type
{
	asset_type_id typeID;

    u32 firstAssetID;
    u32 onePastLastAssetID;
};

struct fea_asset
{
	u64 dataOffset;

    u32 firstTagID;
    u32 onePastLastTagID;
    union
    {
        fea_bitmap_info bitmapInfo;
        fea_sound_info soundInfo;
    };
};

#pragma pack(pop)

#endif