/******************************************************************************
Author: GyuHyeon Lee
Email:  email: weanother@gmail.com
(C) Copyright by GyuHyeon, Lee. All Rights Reserved. $
******************************************************************************/
internal loaded_sound
DEBUGLoadWAV(char *fileName, uint32 sectionFirstSampleIndex, uint32 sectionSampleCount)
{
    loaded_sound result = {};
    Assert(!"NOOOOOO");
    return result;
}

internal loaded_bitmap
DEBUGLoadBMP(char *fileName, v2 alignPercentage = V2(0.5f, 0.5f))
{
    loaded_bitmap result = {};
    Assert(!"NOOOOOO");
    return result;
}

struct load_bitmap_work
{
    game_assets *assets;
    loaded_bitmap *bitmap;
    u32 bitmapID;
    background_task_with_memory *backgroundTask;

    asset_state finalState;
};

// TODO : Try to find where this should be?
internal background_task_with_memory *
BeginBackgroundTaskWithMemory(transient_state *tranState)
{
    background_task_with_memory *foundEmptyTaskSlot = 0;

    for(int taskID = 0;
        taskID < ArrayCount(tranState->backgroundTasks);
        ++taskID)
    {
        background_task_with_memory *task = tranState->backgroundTasks + taskID;

        if(!task->beingUsed)
        {
            foundEmptyTaskSlot = task;
            foundEmptyTaskSlot->beingUsed = true;
            foundEmptyTaskSlot->tempMemory = BeginTemporaryMemory(&foundEmptyTaskSlot->arena);
            break;
        }
    }

    return foundEmptyTaskSlot;
}

inline void
EndBackgroundTaskWithMemory(background_task_with_memory *task)
{
    EndTemporaryMemory(task->tempMemory);

    CompletePreviousWritesBeforeFutureWrites;
    task->beingUsed = false;
}

internal
PLATFORM_WORK_QUEUE_CALLBACK(LoadBitmapWork)
{
    load_bitmap_work *work = (load_bitmap_work *)data;

	fea_bitmap_info *info = &work->assets->assets[work->bitmapID].bitmapInfo;

    work->bitmap->alignPercentage = info->alignPercentage; 
    work->bitmap->width = info->dim[0]; 
    work->bitmap->height = info->dim[1]; 
    work->bitmap->widthOverHeight = (r32)info->dim[0] / (r32)info->dim[1]; 
    work->bitmap->pitch = 4 * info->dim[0]; 
    work->bitmap->memory = (u8 *)work->assets->feaContents + work->assets->assets[work->bitmapID].dataOffset; 

    CompletePreviousWritesBeforeFutureWrites;

    // Actually put inside the bitmap structure 
    work->assets->assetSlots[work->bitmapID].bitmap = work->bitmap;
    work->assets->assetSlots[work->bitmapID].state = work->finalState;

    EndBackgroundTaskWithMemory(work->backgroundTask);
}

internal void
LoadBitmapAsset(game_assets *assets, u32 ID)
{
    if(ID != Asset_Type_Nothing && AtomCompareExchangeUint32((uint32 *)&assets->assetSlots[ID].state, 
                                Asset_State_NotLoaded, 
                                Asset_State_Queued) == Asset_State_NotLoaded)
    {
        background_task_with_memory *backgroundTask = BeginBackgroundTaskWithMemory(assets->tranState);

        if(backgroundTask) 
        {
            load_bitmap_work *work = PushStruct(&backgroundTask->arena, load_bitmap_work);

            work->assets = assets;
            work->bitmapID = ID;
            work->backgroundTask = backgroundTask;
            work->bitmap = PushStruct(&assets->arena, loaded_bitmap);
            work->finalState = Asset_State_Loaded;
            
            platform.AddEntry(assets->tranState->lowPriorityQueue, LoadBitmapWork, work);
        }
    }
}

struct load_sound_work
{
    game_assets *assets;

    loaded_sound *sound;
    u32 soundID;

    background_task_with_memory *backgroundTask;

    asset_state finalState;
};

internal
PLATFORM_WORK_QUEUE_CALLBACK(LoadSoundWork)
{
    load_sound_work *work = (load_sound_work *)data;

    fea_sound_info *info = &work->assets->assets[work->soundID].soundInfo;

    work->sound->channelCount = info->channelCount;
    work->sound->sampleCount = info->sampleCount;
    work->sound->samples[0] = (i16 *)((u8 *)work->assets->feaContents + work->assets->assets[work->soundID].dataOffset);
    work->sound->samples[1] = (i16 *)((u8 *)work->assets->feaContents + work->assets->assets[work->soundID].dataOffset + work->sound->sampleCount*sizeof(i16));

    CompletePreviousWritesBeforeFutureWrites;

    // Actually put inside the bitmap structure 
    work->assets->assetSlots[work->soundID].sound = work->sound;
    work->assets->assetSlots[work->soundID].state = work->finalState;

    EndBackgroundTaskWithMemory(work->backgroundTask);
}

internal void
LoadSoundAsset(game_assets *assets, u32 ID)
{
    if(ID != Asset_Type_Nothing && AtomCompareExchangeUint32((uint32 *)&assets->assetSlots[ID].state, 
                                Asset_State_NotLoaded, 
                                Asset_State_Queued) == Asset_State_NotLoaded)
    {
        background_task_with_memory *backgroundTask = BeginBackgroundTaskWithMemory(assets->tranState);

        if(backgroundTask) 
        {
            load_sound_work *work = PushStruct(&backgroundTask->arena, load_sound_work);

            work->assets = assets;
            work->soundID = ID;
            work->backgroundTask = backgroundTask;
            work->sound = PushStruct(&assets->arena, loaded_sound);
            work->finalState = Asset_State_Loaded;
            
            platform.AddEntry(assets->tranState->lowPriorityQueue, LoadSoundWork, work);
        }
    }
}

internal uint32
GetBestMatchAsset(game_assets *assets, asset_type_id typeID, 
                    asset_match_vector *matchVector, asset_weight_vector *weightVector) // How much I care about the match?
{ 
    real32 bestDiff = Real32Max;

    uint32 bestResult = 0;

    asset_type *assetType = assets->assetTypes + typeID;
    for(uint32 assetID = assetType->firstAssetID;
        assetID < assetType->onePastLastAssetID;
        ++assetID)
    {
        fea_asset *taggedAsset = assets->assets + assetID;

        real32 totalWeightedDiff = 0.0f;

        for(uint32 tagID = taggedAsset->firstTagID;
            tagID < taggedAsset->onePastLastTagID;
            ++tagID)
        {
            fea_tag *tag = assets->tags + tagID;

            real32 a = matchVector->e[tag->ID];
            real32 b = tag->value;

            // If the value is positive, this will work
            real32 d0 = AbsoluteValue(a - b);
            real32 range = assets->tagMaxRange[tag->ID];
            // If the value is negative, this will work
            real32 d1 = AbsoluteValue((a - range * SignOf(a)) - b);

            real32 difference = Minimum(d0, d1);

            real32 weightedDiff = weightVector->e[tag->ID] * difference;
            totalWeightedDiff += weightedDiff;
        }

        if(bestDiff > totalWeightedDiff)
        {
            bestDiff = totalWeightedDiff;
            bestResult = assetID;
        }
    }

    return bestResult;
}

internal u32
GetBestMatchBitmap(game_assets *assets, asset_type_id typeID, 
                    asset_match_vector *matchVector, asset_weight_vector *weightVector) // How much I care about the match?
{
    u32 result = GetBestMatchAsset(assets, typeID, matchVector, weightVector);
    return result;
}

internal u32
GetBestMatchSound(game_assets *assets, asset_type_id typeID, 
                    asset_match_vector *matchVector, asset_weight_vector *weightVector) // How much I care about the match?
{
    u32 result = GetBestMatchAsset(assets, typeID, matchVector, weightVector);
    return result;
}

internal uint32
GetFirstAssetIDFrom(game_assets *assets, asset_type_id assetType)
{
	u32 result = 0;

	asset_type *type = assets->assetTypes + assetType;

	if(type->firstAssetID != type->onePastLastAssetID)
	{
        result = type->firstAssetID;
	}

	return result; 
}

internal u32
GetFirstBitmapFrom(game_assets *assets, asset_type_id assetType)
{
    u32 result = GetFirstAssetIDFrom(assets, assetType);
    return result;
}

internal u32
GetFirstSoundFrom(game_assets *assets, asset_type_id assetType)
{
    u32 result = GetFirstAssetIDFrom(assets, assetType);
    return result;
}

/*
    TODO : 
    platform.ReadFile
    platform.Error
    platform.NoError
    platform.OpenFileBegin
    platform.OpenFile
    platform.OpenFileEnd
*/

internal game_assets *
AllocateGameAssets(transient_state *tranState, memory_arena *arena, memory_index size)
{
    game_assets *gameAssets = PushStruct(arena, game_assets);
    SubArena(&gameAssets->arena, arena, size);

    gameAssets->tranState = tranState;

#if 1
    {
        platform_file_group fileGroup = {};
        fileGroup = platform.OpenFileGroupBegin("fea");

        gameAssets->fileCount = fileGroup.fileCount;
        gameAssets->files = PushArray(&gameAssets->arena, gameAssets->fileCount, asset_file);

        // NOTE : Start from 0
        gameAssets->tagCount = 0;
        gameAssets->assetCount = 0;

        for(u32 fileIndex = 0;
            fileIndex < gameAssets->fileCount;
            ++fileIndex)
        {
            asset_file *assetFile = gameAssets->files + fileIndex;

            assetFile->tagBase = gameAssets->tagCount;
            assetFile->assetBase = gameAssets->assetCount;

            assetFile = platform.OpenFile(&fileGroup, fileIndex);
            assetFile->header = *((fea_header *)platform.ReadFile(&fileGroup, fileIndex, 0));

            if(assetFile->header.magicValue != FEA_MAGIC_VALUE)
            {
                assetFile->handle = platform.Error(assetFile->handle, "Magic value is different.");
            }

            if(assetFile->header.version > FEA_ASSET_VERSION)
            {
                assetFile->handle = platform.Error(assetFile->handle, "Version is higher.");
            }

            if(platform.NoError(assetFile->handle))
            {
                gameAssets->tagCount += assetFile->header.tagCount;
                gameAssets->assetCount += assetFile->header.assetCount;
            }
            else
            {
                InvalidCodePath;
            }
        }

        gameAssets->tags = PushArray(&gameAssets->arena, gameAssets->tagCount, fea_tag);
        gameAssets->assetSlots = PushArray(&gameAssets->arena, gameAssets->assetCount, asset_slot);
        gameAssets->assets = PushArray(&gameAssets->arena, gameAssets->assetCount, fea_asset);

        // NOTE : For each type, loop all the files to get all the tags, types, assets
        for(u32 assetTypeIndex = 0;
            assetTypeIndex < Asset_Type_Count;
            ++assetTypeIndex)
        {
            for(u32 fileIndex = 0;
                fileIndex < gameAssets->fileCount;
                ++fileIndex)
            {
                asset_file *assetFile = gameAssets->files + fileIndex;
                fea_header *header = &assetFile->header;
                fea_tag *feaTags = (fea_tag *)platform.ReadFile(&fileGroup, fileIndex, header->tagOffset);
                fea_type *feaTypes = (fea_type *)platform.ReadFile(&fileGroup, fileIndex, header->assetTypeOffset);
                fea_asset *feaAssets = (fea_asset *)platform.ReadFile(&fileGroup, fileIndex, header->assetInfoOffset);

                // NOTE : Get the tags
                for(u32 tagIndex = 0;
                    tagIndex < header->tagCount;
                    ++tagIndex)       
                {
                    // u32 tagPos = assetFile->tagBase + tagIndex;
                    fea_tag *source = feaTags + tagIndex;
                    fea_tag *dest = gameAssets->tags + assetFile->tagBase + tagIndex;

                    *dest = *source;
                }

                for(u32 assetIndex = 0;
                    assetIndex < header->assetCount;
                    ++assetIndex)
                {            
                    fea_asset *source = feaAssets + assetIndex;
                    fea_asset *dest = gameAssets->assets + assetFile->assetBase + assetIndex;

                    *dest = *source;
                }
            }
        }

        // NOTE : There might be a problem with OS while opening file, so clear the things that we can.
        platform.OpenFileGroupEnd(&fileGroup);
    }

#endif

    // NOTE : Load asset pack file
    debug_read_file_result readResult = platform.DebugPlatformReadEntireFile("test.fea");

    if(readResult.content)
    {
        gameAssets->feaContents = (u8 *)readResult.content;

        fea_header *header = (fea_header *)readResult.content;
        Assert(header->magicValue == FEA_MAGIC_VALUE);
        Assert(header->version == FEA_ASSET_VERSION);

        fea_tag *feaTags = (fea_tag *)((u8 *)readResult.content + header->tagOffset);
        fea_type *feaTypes = (fea_type *)((u8 *)readResult.content + header->assetTypeOffset);
        fea_asset *feaAssets = (fea_asset *)((u8 *)readResult.content + header->assetInfoOffset);

        gameAssets->tagCount = header->tagCount;
        gameAssets->tags = PushArray(&gameAssets->arena, gameAssets->tagCount, fea_tag);
        gameAssets->assetCount = header->assetCount;
        gameAssets->assetSlots = PushArray(&gameAssets->arena, gameAssets->assetCount, asset_slot);
        gameAssets->assets = PushArray(&gameAssets->arena, gameAssets->assetCount, fea_asset);

        for(u32 tagIndex = 0;
            tagIndex < header->tagCount;
            ++tagIndex)
        {
            fea_tag *source = feaTags + tagIndex;
            fea_tag *dest = gameAssets->tags + tagIndex;

            *dest = *source;
        }

        for(u32 assetTypeIndex = 0;
            assetTypeIndex < Asset_Type_Count;
            ++assetTypeIndex)
        {
            fea_type *source = feaTypes + assetTypeIndex;
            asset_type *dest = gameAssets->assetTypes + source->typeID;

            dest->firstAssetID = source->firstAssetID;
            dest->onePastLastAssetID = source->onePastLastAssetID;
        }

        for(u32 assetIndex = 0;
            assetIndex < gameAssets->assetCount;
            ++assetIndex)
        {
            fea_asset *source = feaAssets + assetIndex;
            fea_asset *dest = gameAssets->assets + assetIndex;

            *dest = *source;
        }
    }

    return gameAssets;
}