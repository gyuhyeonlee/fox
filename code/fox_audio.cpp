/******************************************************************************
Author: GyuHyeon Lee
Email:  email: weanother@gmail.com
(C) Copyright by GyuHyeon, Lee. All Rights Reserved. $
******************************************************************************/

internal void
ChangeVolume(playing_sound *playingSound, v2 targetVolume, r32 interpolateDuration)
{
    if(interpolateDuration <= 0.0f)
    {
        playingSound->currentVolume = playingSound->targetVolume = targetVolume;
    }
    else
    {
        playingSound->targetVolume = targetVolume;
        playingSound->dVolumePerSec = (1.0f/interpolateDuration) * (playingSound->targetVolume - playingSound->currentVolume);
    }
}

internal void
ChangePitch(playing_sound *playingSound, r32 pitch)
{
    playingSound->dSample = pitch;
}
        
internal playing_sound *
PlaySound(audio_player *audioPlayer, u32 ID, r32 volume = 1.0f, r32 pitch = 1.0f)
{
    if(!audioPlayer->firstFreePlayingSound)
    {
        audioPlayer->firstFreePlayingSound = PushStruct(audioPlayer->arena, playing_sound);
        audioPlayer->firstFreePlayingSound->next = 0;
    }

    playing_sound *playingSound = audioPlayer->firstFreePlayingSound;
    audioPlayer->firstFreePlayingSound = playingSound->next;

    playingSound->samplesPlayed = 0;
    playingSound->currentVolume = V2(volume, volume);
    playingSound->targetVolume = {};
    playingSound->dVolumePerSec = {};
    playingSound->dSample = pitch;

    playingSound->ID = ID;

    playingSound->next = audioPlayer->firstPlayingSound;
    audioPlayer->firstPlayingSound = playingSound;

    return playingSound;
}

internal void
OutputPlayingSounds(audio_player *audioPlayer, game_sound_output_buffer *soundBuffer, game_assets *assets, memory_arena *arena)
{
    temporary_memory mixerMemory = BeginTemporaryMemory(arena);

    u32 chunkCount = soundBuffer->sampleCount / 4;
    __m128 *realChannel0 = PushArray(mixerMemory.arena, chunkCount, __m128);
    __m128 *realChannel1 = PushArray(mixerMemory.arena, chunkCount, __m128);

    __m128 zero_4x = _mm_set1_ps(0.0f); 
    __m128 one_4x = _mm_set1_ps(1.0f);
    __m128 zeroOneTwoThree_4x;
    GetValue(zeroOneTwoThree_4x, 0) = 0.0f;
    GetValue(zeroOneTwoThree_4x, 1) = 1.0f;
    GetValue(zeroOneTwoThree_4x, 2) = 2.0f;
    GetValue(zeroOneTwoThree_4x, 3) = 3.0f;
    r32 invSamplesPerSec = 1.0f/soundBuffer->samplesPerSecond;

    //
    // NOTE : Clear the samples. 
    //
    __m128 *destq = realChannel0;
    __m128 *destw = realChannel1;
    for(u32 chunkIndex = 0;
        chunkIndex < chunkCount;
        ++chunkIndex)
    {
        _mm_store_ps((float *)destq, zero_4x);
        _mm_store_ps((float *)destw, zero_4x);

        ++destq;
        ++destw;
    }

    // 
    // NOTE : Mix the sounds.
    //
    for(playing_sound **playingSoundPtr = &audioPlayer->firstPlayingSound;
        *playingSoundPtr;
        )
    {
        IACA_START;

        playing_sound *playingSound = *playingSoundPtr; 
        bool32 isSoundFinishedPlaying = false;

        u32 totalChunksToMix = chunkCount;

        __m128 *dest0 = realChannel0;
        __m128 *dest1 = realChannel1;

        __m128 dSampleTimes4_4x = _mm_set1_ps(4.0f*playingSound->dSample);
        __m128 dSample_4x = _mm_set1_ps(playingSound->dSample);

        v2 dVolumePerSample = invSamplesPerSec * playingSound->dVolumePerSec;

        __m128 dVolumePerSample0 = _mm_mul_ps(zeroOneTwoThree_4x, _mm_set1_ps(dVolumePerSample.e[0]));
        __m128 dVolumePerSample1 = _mm_mul_ps(zeroOneTwoThree_4x, _mm_set1_ps(dVolumePerSample.e[1]));

        while(totalChunksToMix && !isSoundFinishedPlaying)
        {
            // TODO : Does this need to be here?
            fea_sound_info *playingSoundInfo = GetSoundInfo(assets, playingSound->ID);
            PrefetchSoundAsset(assets, playingSoundInfo->nextIDToPlay);

            loaded_sound *loadedSound = GetLoadedSound(assets, playingSound->ID);
            if(loadedSound)
            {
                Assert(playingSound->samplesPlayed >= 0.0f);

                u32 chunksToMix = totalChunksToMix; 
                r32 real32ChunkssToMix = (r32)chunksToMix;
                r32 real32ChunksRemainingInSound = ((r32)loadedSound->sampleCount - playingSound->samplesPlayed) / 
                                                    (4.0f*playingSound->dSample);
                u32 chunksRemainingInSound = RoundReal32ToUInt32(real32ChunksRemainingInSound);
                if(chunksToMix > chunksRemainingInSound)
                {
                    chunksToMix = chunksRemainingInSound;
                }

                __m128 samplesPlayed_4x = _mm_set1_ps(playingSound->samplesPlayed);
                __m128 samplePosition = _mm_add_ps(samplesPlayed_4x, _mm_mul_ps(zeroOneTwoThree_4x, dSample_4x));

                // TODO : Stereo!
                // TODO : Safe out of boundary checking
                for(u32 loopIndex = 0;
                    loopIndex < chunksToMix;
                    ++loopIndex)
                {
                    BEGIN_TIMED_BLOCK(FillSoundSample);

                    __m128 volume0 = _mm_add_ps(_mm_set1_ps(playingSound->currentVolume.e[0]), dVolumePerSample0);
                    __m128 volume1 = _mm_add_ps(_mm_set1_ps(playingSound->currentVolume.e[1]), dVolumePerSample1);

                    __m128i sampleIndex = _mm_cvtps_epi32(samplePosition);
                    __m128 t = _mm_sub_ps(samplePosition, _mm_cvtepi32_ps(sampleIndex));

                    // NOTE : Sum all the sounds
                    __m128 sampleValue0 = _mm_set1_ps(0.0f);
                    __m128 sampleValue1 = _mm_set1_ps(0.0f);

                    // TOOD : Stereo!!
                    // __m128 sampleValue0 = loadedSound->samples + sampleIndex;
                    GetValue(sampleValue0, 0) = loadedSound->samples[0][GetValueUint32(sampleIndex, 0)];
                    GetValue(sampleValue0, 1) = loadedSound->samples[0][GetValueUint32(sampleIndex, 1)];
                    GetValue(sampleValue0, 2) = loadedSound->samples[0][GetValueUint32(sampleIndex, 2)];
                    GetValue(sampleValue0, 3) = loadedSound->samples[0][GetValueUint32(sampleIndex, 3)];

                    GetValue(sampleValue1, 0) = loadedSound->samples[0][GetValueUint32(sampleIndex, 0)+1];
                    GetValue(sampleValue1, 1) = loadedSound->samples[0][GetValueUint32(sampleIndex, 1)+1];
                    GetValue(sampleValue1, 2) = loadedSound->samples[0][GetValueUint32(sampleIndex, 2)+1];
                    GetValue(sampleValue1, 3) = loadedSound->samples[0][GetValueUint32(sampleIndex, 3)+1];

                    __m128 invt = _mm_sub_ps(one_4x, t);
                    __m128 resultSampleValue = _mm_add_ps(_mm_mul_ps(invt, sampleValue0), _mm_mul_ps(t, sampleValue1));

                    _mm_storeu_ps((float *)dest0++, _mm_mul_ps(volume0, resultSampleValue));
                    _mm_storeu_ps((float *)dest1++, _mm_mul_ps(volume1, resultSampleValue));

                    samplePosition = _mm_add_ps(samplePosition, dSampleTimes4_4x);
                    playingSound->currentVolume += 4.0f*(1.0f/soundBuffer->samplesPerSecond)*playingSound->dVolumePerSec;

                    END_TIMED_BLOCK(FillSoundSample);
                }

                for(int channelIndex = 0;
                    channelIndex < 2;
                    ++channelIndex)
                {
                    if(playingSound->dVolumePerSec.e[channelIndex] > 0.0f)
                    {
                        playingSound->currentVolume.e[channelIndex] = Clamp(0.0f, playingSound->currentVolume.e[channelIndex], playingSound->targetVolume.e[channelIndex]);
                    }
                    else if(playingSound->dVolumePerSec.e[channelIndex] < 0.0f)
                    {
                        // TODO : Do not use magic number for this!
                        playingSound->currentVolume.e[channelIndex] = Clamp(0.0f, playingSound->currentVolume.e[channelIndex], 1.0f);
                    }
                }

                playingSound->samplesPlayed = GetValue(samplePosition, 3);
                totalChunksToMix -= chunksToMix;

                if(chunksToMix == chunksRemainingInSound)
                {
                    if(playingSoundInfo->nextIDToPlay)
                    {
                        playingSound->ID = playingSoundInfo->nextIDToPlay;
                        // playingSound->samplesPlayed -= (r32)loadedSound->sampleCount;
                        playingSound->samplesPlayed = 0;
                    }
                    else
                    {
                        isSoundFinishedPlaying = true;
                    }
                }

            }
            else
            {
                LoadSoundAsset(assets, playingSound->ID);
                break;
            }

            IACA_END;
        }

        if(isSoundFinishedPlaying)
        {
            // Actually, this is getting rid of the sound from the firstPlayingSound list
            // Therefore, we have to change the value of this, not the pointer
            *playingSoundPtr = playingSound->next;

            playingSound->next = audioPlayer->firstFreePlayingSound;
            audioPlayer->firstFreePlayingSound = playingSound;
        }
        else
        {
            // We should not change the pointer, we should move to the next pointer.
            playingSoundPtr = &playingSound->next;
        }

    }

    //
    // NOTE : Convert to int16 space and put inside the sound buffer
    //
    __m128 *source0 = realChannel0;
    __m128 *source1 = realChannel1;

    __m128i *locToWrite = (__m128i *)soundBuffer->samples;
    for(u32 sampleIndex = 0;
        sampleIndex < chunkCount;
        ++sampleIndex)
    {
        // NOTE : 4 Left Channel samples
        __m128 sample0 = _mm_load_ps((float *)source0++);
        // NOTE : 4 Right Channel samples
        __m128 sample1 = _mm_load_ps((float *)source1++);

        __m128i L = _mm_cvtps_epi32(sample0);
        __m128i R = _mm_cvtps_epi32(sample1);

        __m128i LR0 = _mm_unpacklo_epi32(L, R);
        __m128i LR1 = _mm_unpackhi_epi32(L, R);

        __m128i s01 = _mm_packs_epi32(LR0, LR1);

        *locToWrite++ = s01;
    }

    EndTemporaryMemory(mixerMemory);
}

internal void
InitializeAudioPlayer(audio_player *audioPlayer, memory_arena *arena)
{
    audioPlayer->arena = arena;
    audioPlayer->firstPlayingSound = 0;
    audioPlayer->firstFreePlayingSound = 0;

    audioPlayer->masterVolume = 0.0f;
}