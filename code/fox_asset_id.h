#ifndef FOX_ASSET_ID_H
#define FOX_ASSET_ID_H

enum asset_type_id
{
    // For making empty bitmap
    Asset_Type_Nothing,

    Asset_Type_Square,
    //
    // NOTE : Bitmap types
    //
    Asset_Type_Number,
    Asset_Type_Tree,
    Asset_Type_Shadow,
    Asset_Type_Stairwell,

    Asset_Type_Sword,
    Asset_Type_RaycastAttack,
    Asset_Type_LaserSurrounding,
    // Asset_Type_Attack,

    Asset_Type_Cloud,
    Asset_Type_Sun,
    Asset_Type_Mountain,

    Asset_Type_Head,
    Asset_Type_Cape,
    Asset_Type_Torso,

    Asset_Type_Tutorial,
    Asset_Type_GameLogo,
    Asset_Type_DigipenLogo,
    Asset_Type_TeamLogo,
    Asset_Type_Wave,
    Asset_Type_NextWaveIn,
    Asset_Type_NextWeapon,
    Asset_Type_NextWeaponIndicator,
    Asset_Type_PressR,
    Asset_Type_Credit,

    Asset_Type_ExclamationMark,
    Asset_Type_Arrow,
    Asset_Type_ArrowIndicator,

    Asset_Type_Ship,
    Asset_Type_Propell,
    Asset_Type_Thrust,
    Asset_Type_Aim,
    Asset_Type_Shield,

    //
    // NOTE : Sound Types
    // TODO : Make sound to have 'sound' at the end so that I can recognize more easily.
    //
    
    Asset_Type_Music,
    Asset_Type_BoostSound,
    Asset_Type_LaserSound,
    Asset_Type_EnemyLaserSound,
    Asset_Type_EnemyDeathSound,
    Asset_Type_EnemyAwareSound,

    Asset_Type_Count,
};

enum asset_tag_id
{
    Asset_Tag_FacingDirection,
    Asset_Tag_AnimationIndex,
    Asset_Tag_Sequence,
    Asset_Tag_Numbering,
    Asset_Tag_WeaponType,

    Asset_Tag_Count,
};

#endif