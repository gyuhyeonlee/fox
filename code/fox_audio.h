/******************************************************************************
Author: GyuHyeon Lee
Email:  email: weanother@gmail.com
(C) Copyright by GyuHyeon, Lee. All Rights Reserved. $
******************************************************************************/

#ifndef FOX_AUDIO_H
#define FOX_AUDIO_H

struct playing_sound
{
    u32 ID;

    v2 currentVolume;
    v2 targetVolume;
    v2 dVolumePerSec;

    r32 dSample;

    r32 samplesPlayed;

    playing_sound *next;
};

struct audio_player
{
	memory_arena *arena;
    playing_sound *firstPlayingSound;
    playing_sound *firstFreePlayingSound;

    r32 masterVolume;
};

#endif